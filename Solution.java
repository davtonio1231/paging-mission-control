import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Solution {
    public static void main(String[] args) {
        BufferedReader reader;

        try {
            reader = new BufferedReader(new FileReader(args[0]));
            HashMap<String,String[]> map = new HashMap<>();
            String line = reader.readLine();


            while (line != null) {
                String[] values = line.split("\\|");

                String id = values[1];
                Float rh = Float.valueOf(values[2]);
                Float rl = Float.valueOf(values[5]);
                Float v = Float.valueOf(values[6]);

                if (!map.containsKey(id)) {
                    String[] array = new String[9];
                    Arrays.fill(array,"0");
                    array[0]=id;
                    if (v>rh) {
                        array[1] = "1";
                        array[2] = values[0];
                        array[4] = values[7];
                    } else if (v<rl) {
                        array[5] = "1";
                        array[6] = values[0];
                        array[8] = values[7];
                    }
                    map.put(id,array);
                } else {
                    String[] array = map.get(id);
                    if (v>rh) {
                        if (Integer.parseInt(array[1]) == 0){
                            array[2] = values[0];
                            array[4] = values[7];
                        } else if (Integer.parseInt(array[1]) == 2){
                            array[3] = values[0];
                        }
                        array[1] = String.valueOf(Integer.parseInt(array[1])+1);
                    } else if (v<rl) {
                        if (Integer.parseInt(array[5]) == 0){
                            array[6] = values[0];
                            array[8] = values[7];
                        } else if (Integer.parseInt(array[5]) == 2){
                            array[7] = values[0];
                        }
                        array[5] = String.valueOf(Integer.parseInt(array[5])+1);
                    }
                    map.put(id,array);
                }
                line = reader.readLine();
            }
            reader.close();

            ObjectMapper mapper = new ObjectMapper();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
            ArrayNode json = mapper.createArrayNode();
            
            for (String[] s : map.values()){
                if (Integer.parseInt(s[1])>2){
                    LocalDateTime firstdate = LocalDateTime.parse(s[2], formatter);
                    LocalDateTime lastdate = LocalDateTime.parse(s[3], formatter);
                    Duration duration = Duration.between(firstdate, lastdate);
                    if (duration.compareTo(Duration.ofMinutes(5))<0){
                        ObjectNode entry = mapper.createObjectNode();
                        entry.put("satelliteId",Integer.parseInt(s[0]));
                        entry.put("severity","RED HIGH");
                        entry.put("component",s[4]);
                        String date = s[2];
                        date = date.replace(' ','T');
                        date = date.concat("Z");
                        entry.put("timestamp",date);
                        json.add(entry);
                    }
                } 
                if (Integer.parseInt(s[5])>2){
                    LocalDateTime firstdate = LocalDateTime.parse(s[6], formatter);
                    LocalDateTime lastdate = LocalDateTime.parse(s[7], formatter);
                    Duration duration = Duration.between(firstdate, lastdate);
                    if (duration.compareTo(Duration.ofMinutes(5))<0){
                        ObjectNode entry = mapper.createObjectNode();
                        entry.put("satelliteId",Integer.parseInt(s[0]));
                        entry.put("severity","RED LOW");
                        entry.put("component",s[8]);
                        String date = s[6];
                        date = date.replace(' ','T');
                        date = date.concat("Z");
                        entry.put("timestamp",date);
                        json.add(entry);
                    }
                }
            }

            System.out.println(json.toPrettyString());
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}